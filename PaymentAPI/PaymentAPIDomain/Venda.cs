﻿using PaymentAPI.PaymentAPIContracts.Vendas;

namespace PaymentAPI.PaymentAPIDomain
{
    public class Venda
    {
        public int Id { get; set; }
        public DateTime DataDaVenda { get; set; }
        public Status Status { get; set; }
        public Vendedor Vendedor { get; set; }
        public List<Item> Itens { get; set; }

        public Venda(int id, DateTime dataDaVenda, Vendedor vendedor, List<Item> itens)
        {
            Id = id;
            DataDaVenda = dataDaVenda;
            Status = Status.AguardandoPagamento;
            Vendedor = vendedor;
            Itens = itens;
        }

        public void AtualizaStatusDaVenda(Venda venda, Status novoStatus)
        {
            if (venda.Status == Status.AguardandoPagamento)
            {
                if (novoStatus == Status.PagamentoAprovado)
                    venda.Status = Status.PagamentoAprovado;
                else if (novoStatus == Status.Cancelada)
                    venda.Status = Status.Cancelada;
                else
                    throw new Exception($"Não é permitido realizar a transição de Status De:{venda.Status.ToString()} Para:{novoStatus.ToString()}");
            }
            else if (venda.Status == Status.PagamentoAprovado)
            {
                if (novoStatus == Status.EnviadoParaTransportadora)
                    venda.Status = Status.EnviadoParaTransportadora;
                else if (novoStatus == Status.Cancelada)
                    venda.Status = Status.Cancelada;
                else
                    throw new Exception($"Não é permitido realizar a transição de Status De:{venda.Status.ToString()} Para:{novoStatus.ToString()}");
            } 
            else if (venda.Status == Status.EnviadoParaTransportadora)
            {
                if (novoStatus == Status.Entregue)
                    venda.Status = Status.Entregue;
                else
                    throw new Exception($"Não é permitido realizar a transição de Status De:{venda.Status.ToString()} Para:{novoStatus.ToString()}");
            }
            else
                throw new Exception($"Não é permitido realizar a transição de Status De:{venda.Status.ToString()} Para:{novoStatus.ToString()}");
        }
    }
}
