﻿
using Microsoft.AspNetCore.Mvc;
using PaymentAPI.PaymentAPIContracts.Vendas;
using PaymentAPI.PaymentAPIDomain;

namespace PaymentAPI.PaymentAPIApplication
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private static List<Venda> Vendas = new List<Venda>();
        private static List<Vendedor> Vendedores = new List<Vendedor>();

        public VendaController() { }

        [HttpPost("RegistrarVenda")]
        public IActionResult RegistraVenda(VendaDto vendaDto)
        {
            var vendedor = Vendedores.FirstOrDefault(x => x.Id == vendaDto.Vendedor.Id);
            if (vendedor == null)
            {
                vendedor = new Vendedor(new int(), vendaDto.Vendedor.Email, vendaDto.Vendedor.Cpf, vendaDto.Vendedor.Nome, vendaDto.Vendedor.Telefone);
                Vendedores.Add(vendedor);
            }

            var vendaRealizada = new Venda(new int(), DateTime.Now, vendedor, vendaDto.Itens);
            Vendas.Add(vendaRealizada);

            return Ok(vendaRealizada);
        }
        [HttpGet("ObterPorId/{id}")]
        public IActionResult ObterPorId(int id)
        {
            return Ok(Vendas.FirstOrDefault(x => x.Id == id));
        }
        [HttpPut("AtualizarVenda/{idVenda}")]
        public IActionResult AtualizarVenda(int idVenda, Status status)
        {
            var venda = Vendas.FirstOrDefault(x => x.Id == idVenda);

            venda.AtualizaStatusDaVenda(venda, status);

            return Ok(venda);
        }
    }
}
