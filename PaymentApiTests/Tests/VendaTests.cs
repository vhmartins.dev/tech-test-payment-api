﻿using PaymentAPI.PaymentAPIContracts.Vendas;
using PaymentAPI.PaymentAPIDomain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace PaymentApiTests.Tests
{
    public class VendaTests
    {

        [Fact]
        public void AtualizaStatusDaVenda_Should_UpdateStatusCorreto_DeAguardandoPagamentoParaPagamentoAprovado()
        {
            // Arrange
            var venda = this.ArrangeVenda();

            // Act
            venda.AtualizaStatusDaVenda(venda, Status.PagamentoAprovado);

            // Assert
            Assert.Equal(Status.PagamentoAprovado, venda.Status);
        }

        [Fact]
        public void AtualizaStatusDaVenda_Should_UpdateStatusCorreto_DeAguardandoPagamentoParaCancelada()
        {
            // Arrange
            var venda = this.ArrangeVenda();

            // Act
            venda.AtualizaStatusDaVenda(venda, Status.Cancelada);

            // Assert
            Assert.Equal(Status.Cancelada, venda.Status);
        }

        [Fact]
        public void AtualizaStatusDaVenda_Should_UpdateStatusCorreto_DePagamentoAprovadoParaEnviadoParaTransportadora()
        {
            // Arrange
            var venda = this.ArrangeVenda();
            venda.Status = Status.PagamentoAprovado;

            // Act
            venda.AtualizaStatusDaVenda(venda, Status.EnviadoParaTransportadora);

            // Assert
            Assert.Equal(Status.EnviadoParaTransportadora, venda.Status);
        }

        [Fact]
        public void AtualizaStatusDaVenda_Should_UpdateStatusCorreto_DePagamentoAprovadoParaCancelada()
        {
            // Arrange
            var venda = this.ArrangeVenda();
            venda.Status = Status.PagamentoAprovado;

            // Act
            venda.AtualizaStatusDaVenda(venda, Status.Cancelada);

            // Assert
            Assert.Equal(Status.Cancelada, venda.Status);
        }

        [Fact]
        public void AtualizaStatusDaVenda_Should_UpdateStatusCorreto_DeEnviadoParaTransportadoraParaEntregue()
        {
            // Arrange
            var venda = this.ArrangeVenda();
            venda.Status = Status.EnviadoParaTransportadora;

            // Act
            venda.AtualizaStatusDaVenda(venda, Status.Entregue);

            // Assert
            Assert.Equal(Status.Entregue, venda.Status);
        }


        [Fact]
        public void AtualizaStatusDaVenda_Should_ThrowException_When_NewStatusIsInvalid()
        {
            //Arrange
            var venda = this.ArrangeVenda();
            venda.Status = Status.EnviadoParaTransportadora;

            //Act & Assert
            Assert.Throws<Exception>(() => venda.AtualizaStatusDaVenda(venda, Status.PagamentoAprovado));
        }

        public Venda ArrangeVenda()
        {
            var itens = new List<Item>();
            itens.Add(new Item
            {
                Id = 0,
                Nome = "Iphone",
                Valor = 200
            });


            var vendedor = new Vendedor(new int(), "teste@gmail.com", "50195571886", "Victor Hugo", "(11) 99931-7924");
            return new Venda(new int(), DateTime.Now, vendedor, itens);
        }
    }
}
