﻿using PaymentAPI.PaymentAPIDomain;

namespace PaymentAPI.PaymentAPIContracts.Vendas
{
    public class VendaDto
    {
        public Vendedor Vendedor { get; set; }
        public List<Item> Itens { get; set; }
    }
}
