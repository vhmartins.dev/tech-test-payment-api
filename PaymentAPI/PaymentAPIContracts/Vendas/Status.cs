﻿using System.Runtime.Serialization;

namespace PaymentAPI.PaymentAPIContracts.Vendas
{
    public enum Status
    {
        [EnumMember(Value = "Aguardando Pagamento")]
        AguardandoPagamento,
        [EnumMember(Value = "Pagamento Aprovado")]
        PagamentoAprovado,
        [EnumMember(Value = "Enviado para a Transportadora")]
        EnviadoParaTransportadora,
        [EnumMember(Value = "Entregue")]
        Entregue,
        [EnumMember(Value = "Cancelada")]
        Cancelada
    }
}
