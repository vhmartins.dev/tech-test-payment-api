﻿using PaymentAPI.PaymentAPIDomain;
using Xunit;


namespace PaymentApiTests.Tests
{
    public class VendedorTests
    {
        [Fact]
        public void Teste_ValidaSeAsInformacoesEstaoPreenchidasCorretamente_DeveLancarExcecao_SeEmailNuloOuVazio()
        {
            // Arrange
            var vendedor = new Vendedor();

            // Act & Assert
            Assert.Throws<Exception>(() => vendedor.ValidaSeAsInformacoesEstaoPreenchidasCorretamente(null, "12345678900", "Nome", "999999999"));
            Assert.Throws<Exception>(() => vendedor.ValidaSeAsInformacoesEstaoPreenchidasCorretamente("", "12345678900", "Nome", "999999999"));
            Assert.Throws<Exception>(() => vendedor.ValidaSeAsInformacoesEstaoPreenchidasCorretamente("string", "12345678900", "Nome", "999999999"));
        }

        [Fact]
        public void Teste_ValidaSeAsInformacoesEstaoPreenchidasCorretamente_DeveLancarExcecao_SeCpfNuloOuVazio()
        {
            //Arrange
            var vendedor = new Vendedor();

            //Act & Assert
            Assert.Throws<Exception>(() => vendedor.ValidaSeAsInformacoesEstaoPreenchidasCorretamente("email@teste.com", null, "Nome", "999999999"));
            Assert.Throws<Exception>(() => vendedor.ValidaSeAsInformacoesEstaoPreenchidasCorretamente("email@teste.com", "", "Nome", "999999999"));
            Assert.Throws<Exception>(() => vendedor.ValidaSeAsInformacoesEstaoPreenchidasCorretamente("email@teste.com", "string", "Nome", "999999999"));
        }

        [Fact]
        public void Teste_ValidaSeAsInformacoesEstaoPreenchidasCorretamente_DeveLancarExcecao_SeNomeNuloOuVazio()
        {
            //Arrange
            var vendedor = new Vendedor();

            //Act & Assert
            Assert.Throws<Exception>(() => vendedor.ValidaSeAsInformacoesEstaoPreenchidasCorretamente("email@teste.com", "12345678900", null, "999999999"));
            Assert.Throws<Exception>(() => vendedor.ValidaSeAsInformacoesEstaoPreenchidasCorretamente("email@teste.com", "12345678900", "", "999999999"));
            Assert.Throws<Exception>(() => vendedor.ValidaSeAsInformacoesEstaoPreenchidasCorretamente("email@teste.com", "12345678900", "string", "999999999"));
        }

        [Fact]
        public void Teste_ValidaSeAsInformacoesEstaoPreenchidasCorretamente_DeveLancarExcecao_SeTelefoneNuloOuVazio()
        {
            //Arrange
            var vendedor = new Vendedor();

            //Act & Assert
            Assert.Throws<Exception>(() => vendedor.ValidaSeAsInformacoesEstaoPreenchidasCorretamente("email@teste.com", "12345678900", "Nome", null));
            Assert.Throws<Exception>(() => vendedor.ValidaSeAsInformacoesEstaoPreenchidasCorretamente("email@teste.com", "12345678900", "Nome", ""));
            Assert.Throws<Exception>(() => vendedor.ValidaSeAsInformacoesEstaoPreenchidasCorretamente("email@teste.com", "12345678900", "Nome", "string"));
        }
    }
}
