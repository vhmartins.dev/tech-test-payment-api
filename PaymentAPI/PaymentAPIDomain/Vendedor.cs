﻿namespace PaymentAPI.PaymentAPIDomain
{
    public class Vendedor
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Cpf { get; set; }
        public string Nome { get; set; }
        public string Telefone { get; set; }

        public Vendedor(){}
        public Vendedor(int id, string email, string cpf, string nome, string telefone)
        {
            this.ValidaSeAsInformacoesEstaoPreenchidasCorretamente(email, cpf, nome, telefone);
            Id = id;
            Email = email;
            Cpf = cpf;
            Nome = nome;
            Telefone = telefone;
        }
        public void ValidaSeAsInformacoesEstaoPreenchidasCorretamente(string email, string cpf, string nome, string telefone)
        {
            if(string.IsNullOrEmpty(email) || email.Equals("string"))
                throw new Exception("O campo Email está incorreto, deve preencher corretamente!");
            else if(string.IsNullOrEmpty(cpf) || cpf.Equals("string"))
                throw new Exception("O campo Cpf está incorreto, deve preencher corretamente!");
            else if(string.IsNullOrEmpty(nome) || nome.Equals("string"))
                throw new Exception("O campo Nome está incorreto, deve preencher corretamente!"); 
            else if(string.IsNullOrEmpty(telefone) || telefone.Equals("string"))
                throw new Exception("O campo Telefone está incorreto, deve preencher corretamente!");
        }
    }
}
